<?php

class Users
{
    private $users;

    public function __construct()
    {
        $this->users = [];
    }

    public function addUser(string $fname, string $lname, string $mname="", string $birth="", string $cmmnt="")
    {
        $this->users[] = new User($fname, $lname, $mname, $birth, $cmmnt);
    }

    public function addNewUser(User $user)
    {
        $this->users[] = $user;
    }

    public function printUsers()
    {
        echo "<table>";
        echo "<tr>";
        echo "<td>fname</td>";
        echo "<td>lname</td>";
        echo "<td>mname</td>";
        echo "<td>birth</td>";
        echo "<td>cmmnt</td>";
        echo "</tr>";
        echo "\n";

        foreach ($this->users as $user) {
            $u = $user->getUser();
            echo "<tr>";
            echo "<td>".$u->fname."</td>";
            echo "<td>".$u->lname."</td>";
            echo "<td>".$u->mname."</td>";
            echo "<td>".$u->birth."</td>";
            echo "<td>".$u->cmmnt."</td>";
            echo "<tr>";
            echo "\n";
        }
        echo "</table>";
        echo "\n";
        echo "\n";
    }
}
