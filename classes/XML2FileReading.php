<?php

class XML2FileReading extends XMLFileReading
{
    protected $assoc = [
        "fname"      => "fname",
        "lname"      => "lname",
        "mname"      => "mname",
        "birth_date" => "birth",
        "comment"    => "cmmnt",
    ];

    public function ReadFile()
    {
        return $this->ReadFileWhenKeysIn(false);
    }
}
