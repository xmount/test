<?php

class XMLFileReading extends FileReading
{
    protected $assoc = [];

    protected function ReadFileWhenKeysIn(bool $tags=false)
    {
        parent::ReadFile();
        $users = new Users();
        $result = simplexml_load_file($this->fname);
        foreach ($result->worker as $w) {
            $user = new User();
            foreach ($w as $attr => $p) {
                if ($tags) {
                    $attr = (string)$p["name"];
                }
                if (isset($this->assoc[$attr])) {
                    $property = $this->assoc[$attr];
                    $user->$property = (string)$p;
                }
            }
            $users->addNewUser($user);
        }
        return $users;
    }
}
