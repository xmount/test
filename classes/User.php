<?php

class User
{
    public $fname;
    public $lname;
    public $mname;
    public $birth;
    public $cmmnt;

    public function __construct(string $fname="", string $lname="", string $mname="", string $birth="", string $cmmnt="")
    {
        $this->fname = $fname;
        $this->lname = $lname;
        $this->mname = $mname;
        $this->birth = $birth;
        $this->cmmnt = $cmmnt;
    }

    public function __set($property, $value)
    {
        if (property_exists($this, $property)) {
            $this->$property = $value;
        }

        return $this;
    }

    public function getUser()
    {
        return $this;
    }
}
