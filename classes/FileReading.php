<?php

class FileReading
{
    protected $fname = null;

    public function __construct(string $fname)
    {
        $this->fname = $fname;
    }

    public function ReadFile()
    {
        if (empty($this->fname) || !file_exists($this->fname)) {
            return null;
        }
    }
}
