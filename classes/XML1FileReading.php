<?php

class XML1FileReading extends XMLFileReading
{
    protected $assoc = [
        "first_name"  => "fname",
        "last_name"   => "lname",
        "middle_name" => "mname",
        "birth_date"  => "birth",
        "comment"     => "cmmnt",
    ];

    public function ReadFile()
    {
        return $this->ReadFileWhenKeysIn(true);
    }
}
