<?php

class CSVFileReading extends FileReading
{
    public function ReadFile()
    {
        parent::ReadFile();
        $users = new Users();
        $f = fopen($this->fname, "rt");
        while ($data = fgetcsv($f, 65000, ";")) {
            $users->addUser($data[0], $data[1], $data[2], $data[3], $data[4]);
        }
        fclose($f);
        return $users;
    }
}
