<?php

spl_autoload_register(function($classname){
	require_once( "classes/".$classname . ".php");	
});

$file = $_GET["file"];

if ($file =="csv"){
	$reader3 = new CSVFileReading("csv.csv");
	$users3 = $reader3->ReadFile();
	$users3->printUsers();
}

if ($file == "xml1"){
	$reader1 = new XML1FileReading("xml1.xml");
	$users1 = $reader1->ReadFile();
	$users1->printUsers();
}

if ($file == "xml2"){
	$reader2 = new XML2FileReading("xml2.xml");
	$users2 = $reader2->ReadFile();
	$users2->printUsers();
}